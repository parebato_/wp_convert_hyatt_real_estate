<?php get_header(); ?>



<?php 


	/*Template Name: Login Page*/


 ?>

		<div id="signInPage">

					<div class="row">

						<div class="col-lg-6 sign_up banner center">
							<div class="text-center">
								<h1 class="">New to Hyatt? Create an account now!</h1>

								<a class="btn btn-light my-4" href="register.php"> Sign up now</a>
							</div>
						</div>

						<div class="col-lg-6 center">
							<div class="login">

								<div >

										<h4>Login to your account or <a href="index.php">Visit website</a></h4>

										<hr>

									<form 
										id="loginForm" 
										method="post"
										enctype="multipart/form-data"
										action="login.php">

									<div class="form-group">
										<label 
												for="login_username" 
												class="control-label">
												Username
										</label>

										<input 
											type="text" 
											class="form-control" 
											id="login_username"
											name="login_username"  
											required
											onblur="validateUsername()" 
											value="" >
											

										<small ><i id="username_error"></i></small>
									 </div>

									<div class="form-group">
										<label 
											for="login_password" 
											class="control-label">
											Password
										</label>

										<input 
											type="password" 
											class="form-control" 
											id="login_password" 
											name="login_password" 
											value="" 
											required
											onblur="validatePassword()" >

										 <small ><i id="password_error"></i></small>

										</div>

										<div class="form-group d-flex custom-checkbox">

											<label for="remember_me">Remember me</label>
											<input
											 	id="remember_me"		
												name="remember_me" 
												type="checkbox">

										</div>

										<button 
											type="submit" 
											class="btn btn-light" 
											name="login">
											Login
										</button>

									<a 
										id="password_reset" 
										class=" m-4" href="#">
										Forgot your password?
									</a>


									</form>

									
								</div>
								<!-- End of #login_form  -->
							</div>
						</div>
					</div>
				</div>

<?php get_footer(); ?>