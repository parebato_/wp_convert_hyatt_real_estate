<?php

 add_action('rest_api_init', 'property_likes_route');

 function property_likes_route(){

 	register_rest_route('hyatt/v1', 'likes', array(
 		'methods'	=> 'POST',
 		'callback'	=>'create_like'
 	));


 		register_rest_route('hyatt/v1', 'likes', array(
 		'methods' 	=> 'DELETE',
 		'callback' 	=> 'delete_like'
 	));

 }




 function create_like($data){	

	 	if(is_user_logged_in()){

		 	$property = sanitize_text_field($data['property_id']);
		 	//"property_id" here comes from like.js ajax "data" value

		 	$exist_query			= new WP_Query(array(
				'author' 				=> get_current_user_id(),
				'post_type' 		=> 'likes',
				'meta_query'	 	=> array(
					array(
						'key' 			=> 'liked_property_id',
						'compare' 	=> '=',
						'value'			=> $property
					)
				)
			));

		 	if($exist_query->found_posts == 0 
		 		AND get_post_type($property) == 'listings'){ 
		 	//if user have not liked the post yet

		 		return
		 		wp_insert_post(array(
			 		'post_type' => 'likes',
			 		'post_status' => 'publish',
			 		'post_title' => 'Like',
			 		'meta_input' => array(
			 			'liked_property_id'  => $property
			 		)
			 	));

		 	} else { //	if($exist_query->found_posts == 0 ) else bracket

		 		 die ("You have already liked this property");
		 	}

		

	 	} else { // if(is_user_logged_in() else bracket

	 		die ("You need be logged in to create a like");

	 	}

 } //create_like() end bracket







 function delete_like($data){

 	$like = sanitize_text_field($data['like']);

 	/*
	Check to validate if... 
		1. Current user matches the id of the user who made the post in this case "like"

		2. Post to be deleted is "like" post type.
 	*/


		if (
			get_current_user_id() == get_post_field('post_author', $like) AND
			get_post_type($like) == 'likes'
		) {

			wp_delete_post($like, true);
			return 'You have unliked this page';

		} else {

			exit();
		}

 } //delete_like()






/*

Create Note: 

1. To add the Page/Property Id in the Likes Post page, use the' meta_input'

2. if(is_user_logged_in) then he can only like a post

	a. Make sure nonce is included

3. User can only like once per property page
	
	a. use the $exist_query from single.php
	b. replace the query array value to the property id var
	c. add additional query to make sure page ID's liked is 100% correct/legit --get_post_type()
*/