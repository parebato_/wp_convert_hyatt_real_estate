<?php



/*
==========================
Remove login tooltip
==========================
*/

function wpTollTip(){
	return get_bloginfo('name');
}

add_filter('loging_headertitle', 'wpTollTip');


/*
==========================
WP logo redirect
==========================
*/

function wpLogoUrl(){
	return esc_url(site_url('/'));
}

add_action('login_headerurl', 'wpLogoUrl');




/*
==========================
remove wp admin bar
==========================
*/

function removeAdminBar(){
	$user = wp_get_current_user();
	if(count($user->roles) == 1 AND $user->roles[0] == 'subscriber'){
		show_admin_bar(false);		
	}
}

add_action('wp_loaded', 'removeAdminBar');




/*
==========================
subscriber : redirect on login
==========================
*/


function subsRedirect(){
	$user = wp_get_current_user();

	if(count($user->roles) == 1 AND $user->roles[0] == 'subscriber'){
		wp_redirect(site_url('/'));
		exit;
	}

}

add_action('admin_init', 'subsRedirect');




/*
==========================
Remove WP version info
==========================
*/

function remove_WPVersion(){
	return '';
}

add_filter('the_generator', 'remove_WPVersion');

