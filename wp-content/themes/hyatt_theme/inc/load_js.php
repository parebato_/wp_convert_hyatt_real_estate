<?php


function load_js(){

	wp_enqueue_script(
		'nav', 
		get_template_directory_uri() . '/js/nav.js', 
		array('jquery'),
		'1.0.0',
		true

	);


	wp_enqueue_script(
		'likes', 
		get_template_directory_uri() . '/js/likes.js', 
		array('jquery'),
		'1.0.0',
		true

	);

	wp_localize_script(
		'likes', 
		'hyatt_data',
		 array(
			'root_url' => get_site_url(),
			'nonce' =>	wp_create_nonce('wp_rest')
	));

}


add_action('wp_enqueue_scripts', 'load_js');
