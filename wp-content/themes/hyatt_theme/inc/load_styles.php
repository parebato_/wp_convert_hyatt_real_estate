<?php 


function hyatt_css(){


	wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');

	wp_enqueue_style('typekit-fonts', 'https://use.typekit.net/cqk2gsw.css');

	wp_enqueue_style('f-a', 'https://use.fontawesome.com/releases/v5.6.3/css/all.css');


	wp_enqueue_style('main', get_template_directory_uri() . '/css/style.css', null, microtime() );
	wp_enqueue_style('mobile', get_template_directory_uri() . '/css/mobile.css', null, microtime());
	wp_enqueue_style('revise', get_template_directory_uri() . '/css/revise.css', null, microtime());
}

add_action('wp_enqueue_scripts', 'hyatt_css');
