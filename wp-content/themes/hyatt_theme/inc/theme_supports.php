<?php 

function hyatt_theme_features(){

	//Image sizes
	add_image_size('bannerImage', 2000, 800, true);
	add_image_size('property_thumbnail', 600, 400, true); 
	add_image_size('neighbour_image', 745, 559, true); 
	add_image_size('floorplan_image', 514, 411, true); 
	add_image_size('carousel', 900, 500, true); 

	add_theme_support('title-tag');
	add_theme_support('menus');
	add_theme_support('post-thumbnails');
	add_theme_support( 'html5', 
		array(
		'comment-list', 
		'comment-form', 
		'search-form'
		) 
	);
}

add_action('after_setup_theme', 'hyatt_theme_features');