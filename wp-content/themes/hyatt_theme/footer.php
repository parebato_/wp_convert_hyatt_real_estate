<section class="home home_housing">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-6">
				<div class="icon">
					<img src="<?php bloginfo('template_directory'); ?>/img/home/icon01.png" alt="">
				</div>

				<div class="caption">
					<h2>Housing Association?</h2>
					<p>Like you, we want people to find the right homes.Let's work together to achieve this.</p>
					<a href="">VIEW OUR AWESOME TIPS</a>
				</div>
			</div>

			<div class="col-sm-6 row-content">
				<div class="icon">
					<img src="<?php bloginfo('template_directory'); ?>/img/home/icon02.png" alt="">
				</div>
				<div class="caption">
					<h2>Selling your home?</h2>
					<p>Have a chat with us. Our friendly and helpful advisers will help you make this move.</p>
					<a href="">VIEW OUR RESOURCES</a>
				</div>
			</div>
		</div>
	</div>
</section>


<footer>
	<div class="container">
		<div class="row links">

			<div class="col-lg-3 col-md-12 copyright">
				<a href="home.php"><img src="<?php bloginfo('template_directory'); ?>/img/home/Hyatt_logo.png" alt=""></a>
				<p>Copyright Hyatt Homes 2019</p>
			</div>

		
			<div class="f-links col-lg-3 col-sm-4">
				<h2>Useful Links</h2>
				<li><a href="<?= site_url('/about'); ?>">About</a></li>
				<li><a href="">Terms & Conditions</a></li>
				<li><a href="<?= site_url('/properties'); ?>">Properties</a></li>
			</div>

			<div class="f-links col-lg-3 col-sm-4">
				<h2>Useful Links</h2>
				<li><a href="about.php">About Us</a></li>
				<li><a href="">Terms & Conditions</a></li>
				<li><a href="properties.php">Properties</a></li>
			</div>

			<div class="f-links col-lg-3 col-sm-4">
				<h2>Get in touch</h2>
				<li><a href="#">13-14 Orchard Street</a></li>
				<li><a href="#">Orchard Street Business Centre</a></li>
				<li><a href="#">+44 7878 278 7803</a></li>
			</div>
		</div>
</div>

	<hr class="line">

	<div class="icon-container">
		<div class="row">
			<div class="col-md-3 col-sm-6 icon">
				<a class=" mr-3" href=""><img src="<?php bloginfo('template_directory'); ?>/img/home/facebook.png" alt=""></a>
				<a class=" mr-3" href=""><img src="<?php bloginfo('template_directory'); ?>/img/home/twitter.png" alt=""></a>
				<a class=" mr-3" href=""><img src="<?php bloginfo('template_directory'); ?>/img/home/ig.png" alt=""></a>
			</div>
			<div class="cs col-md-6 col-sm-6">
				Web design London by <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/home/CS.png" alt=""></a>
			</div>
		</div>
	</div>
</footer>



<!-- Bootstrap core JavaScript -->

<?php wp_footer(); ?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 
</body>

</html>
