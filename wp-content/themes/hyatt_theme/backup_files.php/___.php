


 <!-- saved property block -->


<div class="col-md-4">

	<div class="block">
		<a class="d-flex" href="">

			<img src="<?php echo get_theme_file_uri( 'img/properties/floorplan.png'); ?>">

			<div class="details">
				<h4 class="title my-1">AirBnB – 2 Floors 2 Bedroom</h4>
				<p class="mb-2"> Status: <span class="green">Available</span> </p>					
			</div>
		</a>					
	</div>

</div>






<!-- SAved for liked properties -->

<section class="profile">
	
	<div class="container">

		<h2>Liked Properties</h2>

		<div class="liked_properties">

			<div class="row">




				<div class="col-md-4">

					<div class="block">
						<a class="d-flex" href="">

							<img src="<?php echo get_theme_file_uri( 'img/properties/floorplan.png'); ?>">

							<div class="details">
								<h4 class="title my-1"></h4>
								<p class="mb-2"> Status: <span class="green">Available</span> </p>					
							</div>
						</a>					
					</div>

				</div>





		
			</div>						
		</div>
	</div>
</section>



<?php

$shops = new WP_Query(
    array(
        'posts_per_page' => -1,
        'post_type' => 'shop'
    )
);

if ( $shops->have_posts() ) {
    while ( $shops->have_posts() ) {
        $shops->the_post();
        ...
        $shop_id = get_post_meta( $post->ID, 'shop-id', true );
        $restaurants = new WP_Query(
            array(
                'posts_per_page' => -1,
                'post_type' => 'restaurant',
                'meta_query' => array(
                    array(
                        'key' => 'restaurant-id',
                        'value' => $shop_id,
                        'compare' => '='
                    )
                )
            )
        );
        if ( $restaurants->have_posts() ) {
            while ( $restaurants->have_posts() ) {
                $restaurants->the_post();
                ...
            }
        }
    }
}


/*
===============================================================
What for? -- 
===============================================================
*/