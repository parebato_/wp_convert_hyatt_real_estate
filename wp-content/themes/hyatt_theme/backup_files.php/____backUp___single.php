<?php  
	get_header();

	/*
	Template Name: Single Properties
	*/
?>


<div class="properties">

	<?php 

	$list = get_field('single_properties');
	$city = $list['city'];
	$like_number = get_field('like_number');
	


		while(have_posts()){
					the_post();
					
		?>

				<section 
					class="banner properties-banner"
					style="background-image: url(<?= $list['banner_image']['url']; ?>);">

					<div class="banner_caption_container">

						<div class="banner_caption">
							<span class="head"><?= $list['city']; ?></span>
							<h1><?php the_title(); ?></h1>		
						</div>

					</div>
				</section>

						<?php 
				}
	 ?>



		<!-- *********END OF BANNER ********* -->


		<section class="properties-intro r">

			<div class="nav-container">
				<nav class="navbar sticky nav-hidden  navbar-expand-sm navbar-light">

					<div id="stickyNav" class="collapse navbar-collapse">

						<ul class="navbar-nav">
							<li class="nav-item">
								<a class="nav-link" href="#information">Information <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link short" href="#">Location</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#outside">What's outside?</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#neighbour">Neighbourhood</a>
							</li>
						</ul>

						<div class="downloads">
							<div class="dwn">
								<a class="dwn-link" href="">
									<img class="poly" src="<?= get_theme_file_uri('/img/properties/dwnlds-bg.png'); ?>">
									Downloads
									<img class="arrow arrow1" src="<?= get_theme_file_uri('/img/properties/dwnlds-arrow.png'); ?>">
									<img class="arrow arrow2" src="<?= get_theme_file_uri('/img/properties/dwnlds-arrow2.png'); ?>">
								</a>
							</div>


							<div class="drop-menu">
								<a class="dropdown-item " href="#">PDF</a>
								<a class="dropdown-item " href="#">Floor Plan</a>
							</div>

						</div>
					</div>
				</nav>


			</div>

			<div class="caption">
				<p><?= $list['main_content']; ?></p>
						
				<div class="arrow arrow2 a-position">
					<img src="<?= get_theme_file_uri('/img/properties/arrow2.png'); ?>">
				</div>

			</div>

			<hr class="line">

		</section>



<!-- Cost Details -->

		<section class="price">

			<div class="container d-flex">

				<div class="status d-flex">

					<p>Scheme : <strong><?php echo $list['scheme']; ?></strong></p> 	

	<?php 
			$scheme = $list['scheme'];
			//Only display Shared Price if Scheme is set to Shared Ownership

		if($scheme == "Shared Ownership") {?>			
					<p>Share Price : <strong>£<?php echo $list['share_price']; ?></strong></p> 	
			<?php										
	} ?>


	<?php  // Set color to red if Status is set to Sold
			$status = $list['status'];
			
			if($status == "Sold"){ ?>
				
					<p>Status : <strong class="red"><?php echo $list['status']; ?></strong></p>
				<?php

				}else { ?>

					<p>Status : <strong class="green"><?php echo $list['status']; ?></strong></p>
				<?php
		} ?>			

				</div>


				<div class="costs d-flex">	
					<p>Full Value : <strong> £<?php echo $list['full_value']; ?></strong></p>

					<p>Minimum Deposit : <strong> £<?php echo $list['min_deposit']; ?></strong></p>
				</div>



				<div class="like-btn">
					<?php 
						$likes 						= new WP_Query(array(
							'post_type' 		=> 'likes',
							'meta_query'	 	=> array(
								array(
									'key' 			=> 'liked_property_id',
									'compare' 	=> '=',
									'value'			=> get_the_ID()
								)
							)
						));


							//Query to check and allow one like per post for logged in user

							$exist_status = 'no';


						if(is_user_logged_in()) {


							$exist_query			= new WP_Query(array(
								'author' 				=> get_current_user_id(),
								'post_type' 		=> 'likes',
								'meta_query'	 	=> array(
									array(
										'key' 			=> 'liked_property_id',
										'compare' 	=> '=',
										'value'			=> get_the_ID()
									)
								)
							));

						
							if($exist_query->found_posts){
								$exist_status = 'yes';
							} 


					if($exist_status == 'yes'){ ?>
			
					<p class="liker" 
						data-exists="<?php echo $exist_status; ?>"
						data-property="<?php the_ID(); ?>"
						data-like="<?php echo $exist_query->posts[0]->ID; ?>">

						<i class="liked far fa-thumbs-up" aria-hidden="true"></i>
						<span class="like-count"><?php echo $likes->found_posts; ?></span>
					</p>
				
					<?php

					} else { ?>
			
					<p class="liker" 
						data-exists="<?php echo $exist_status; ?>"
						data-property="<?php the_ID(); ?>">
							
						<i class="far fa-thumbs-up" aria-hidden="true"></i>
						<span class="like-count"><?php echo $likes->found_posts; ?></span>
					</p>
					
					<?php 
				} // end of if($exist_status == 'yes')

				
			}  else { 
				 /*
					if (is_user_logged_in) else bracket	
					user not logged in
				*/ ?>
				
						<p class="liker" 
							data-property="<?php the_ID(); ?>">				
							<i class="far fa-thumbs-up" aria-hidden="true"></i>
							<span class="like-count"><?php echo $likes->found_posts; ?></span>
						</p>	

					<?php
		} // end of if (is_user_logged_in)
			?>

				</div>
			</div>
		</section>

	<hr>
		
		




		<section class="property-slides">
			<div class="container">
				<span class="head"><?php echo $city; ?></span>
				<h3 class="caption-main">What's inside</h3>

		<?php 
			 $sliders = get_field('sliders');

		?>
		 	
				<div id="indicators" class="carousel slide" data-ride="carousel">
				
					<div class="carousel-inner">
					
						 <div class="carousel-item active">
							<img  class="carousel-img" src="<?php echo $sliders['image4']['url']; ?>" alt="First slide">
						</div>

	<?php  

	 if($sliders){

	 	foreach ($sliders as $slider){
	 		$slide = $slider['url'];

	 			//if no image uploaded set image3 as a default;

			 		if(!$slide){ 

	?>
						<div class="carousel-item">
							<img class="carousel-img" src="<?php echo $sliders['image3']['url']; ?>">
						</div>
	<?php 

			 		} else {

	?>

		 				<div class="carousel-item">
		 					<img  class="carousel-img" src="<?php echo $slide; ?>">
		 				</div>					
	<?php 

			 		}
			 	} 
		 }
		 	
	?>

						<a class="carousel-control-prev carousel-control" href="#indicators" role="button" data-slide="prev">
							<span aria-hidden="true">
								<img src="<?= get_theme_file_uri('/img/properties/prev.png'); ?>" alt="next">
							</span>
						</a>

						<a class="carousel-control-next carousel-control" href="#indicators" role="button" data-slide="next">
							<span aria-hidden="true">
								<img src="<?= get_theme_file_uri('/img/properties/next.png'); ?>" alt="prev">
							</span>
						</a>

					</div>

				</div>
			</div>
			<hr class="line">
		</section>




		<section id="information" class="room-info">
			<div class="content-wrapper">
				<div class="row">
					<div class="col-md-6 caption">

						<h2 class="caption-main">
							<?= $list['floor_plan']['intro']; ?>
						</h2>
						
						<p>
						<?= $list['floor_plan']['details']; ?>
						</p>

						<a class="btn btn-light" href="">
							<span>View the floorplan</span>
							<img src="<?= get_theme_file_uri('/img/properties/search.png'); ?>">
						</a>
					</div>
					<div class="col-md-6 room">
							<img src="<?= $list['floor_plan']['image']['url']; ?>">
					</div>

				</div>
			</div>
		</section>
		<hr class="line">



		<section id="outside" class="outside">
			<div class="caption-container">
				<div class="caption">
					<span class="head">
						<?php echo $city; ?>
					</span>
					<h2 class="caption-main">
						What's outside
					</h2>
					<p>
						<?php echo $list['outside']['intro']; ?>
					</p>

					<div >
						<ul class="list text-center">
							<li><?php echo $list['outside']['details']; ?></li>				
						</ul>
					</div>
				</div>
			</div>
			<hr class="line">
		</section>



		<section id="neighbour" class="neighbour r">
			<div class="content-wrapper">
				<div class="row">
					<div class="elipse a-positino">
						<img src="<?= get_theme_file_uri('/img/properties/elipse.png'); ?>">
					</div>
					<div class=" col-md-6 caption">
						<span class="head">
							<?php echo $city; ?>
						</span>
						<h2 class="caption-main">
							The neighbourhood
						</h2>
						<p>
							<?php echo $list['neighbourhood']['details']; ?>
						</p>

					</div>
					<div class="col-md-6">
						<div class="neighbour_neighbour">
							<img class="" src="<?php echo $list['neighbourhood']['image']['url']; ?>">
						</div>
					</div>
				</div>
			</div>
		</section>



		<section class="eligibility">
			<div class="caption-container">
				<div class="caption">
					<span class="head">FIND OUT ABOUT</span>
					<h2 class="caption-main">
						Are you eligible?
					</h2>

					<p>
						<span class="sign">+</span> Live or work in the borough
						<br>
						<span class="sign">+</span> First time buyer
						<br>
						<span class="sign">+</span> Earn under the Mayor of London’s income threshold for affordable housing
						<br>

					</p>
				</div>
			</div>
		</section>


		<section class="next_steps">
			<p>What are my next steps <span class="sign"> + </span></p>
		</section>

</div>



<?php get_footer(); ?>
