	<?php 

	get_header();

	$banner = get_field('banner');
	$aim = get_field('aim');
	$values = get_field('values');

	 ?>


		<div class="about">
		<section 
			class="banner about_banner"
			style="background-image: url(<?= $banner['image']['url']; ?>);">

			<div class="banner_caption_container">

				<div class="banner_caption"> 
					<span class="head">ABOUT US</span>
					<h1>
						<?= $banner['heading']; ?>
					</h1>
					<p>
						<?= $banner['caption']; ?>
					</p>
				</div>

				<div class="scroll-container">
					<div class="scroll a-position">
						<img src="<?=get_theme_file_uri('img/services/arrow0.png') ?>" alt="">
					</div>
				</div>

			</div>

		</section>


		<!-- *********END OF BANNER ********* -->




		<section class="aim">
			<div class="arrow1 a-position">
				<img class="img-fluid" src="<?=get_theme_file_uri('img/About/angle_up.png') ?>" alt="">
			</div>
			<div class="container">
				<div class="caption text-center">

					<?= $aim['heading']; ?>
				
					<p>  <?= $aim['caption']; ?>  	</p>
				
				</div>
			</div>
			<hr class="line">
		</section>





		<section class="values">
			<div class="caption head">
				<h2>
					Our core values
				</h2>
				<p>
					As an ethical agency, everything we do stems from our three core values.
				</p>
			</div>

			<div class="arrow2 a-position">
				<img class="img-fluid" src="<?=get_theme_file_uri('img/About/angle_side.png') ?>" alt="">
			</div>

			<div class="caption-container">
				<div class="row justify-content-center">
					<div class="col-lg-4 col-md-6">
						<div class="caption">
							<h2><?= $values['1st_value']['title']; ?></h2>
							<p> <?= $values['1st_value']['content']; ?></p>
							
						</div>
					</div>

					<div class="col-lg-4 col-md-6">
						<div class="caption">
							<h2><?= $values['2nd_value']['title']; ?></h2>
							<p> <?= $values['2nd_value']['content']; ?></p>
							
						</div>
					</div>

					<div class="col-lg-4 col-md-6">
						<div class="caption">
							<h2><?= $values['3rd_value']['title']; ?></h2>
							<p><?= $values['3rd_value']['content']; ?> 	</p>
						
						</div>
					</div>

				</div>
			</div>
		</section>



		<section class="partners">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 banner part1">
						<div class="caption">
							<h2>Our partners </h2>
							
							<p>
								If you already have a charity that’s close to your heart, let us know, and we’ll <br> make sure that they receive the percentage instead.
							</p>
							<a class="head" href="#">LEARN MORE</a>
						</div>
					</div>

					<div class="col-lg-6 banner part2">
						<div class="caption">
							<h2>A new approach to buying and selling property.
							</h2>
							<p>
								If you already have a charity that’s close to your heart, let us know, and we’ll <br> make sure that they receive the percentage instead.
							</p>
							<a class="head" href="#">LEARN MORE</a>
						</div>
					</div>
				</div>
			</div>
		</section>


	</div>



		<?php get_footer(); ?>

		<?=get_theme_file_uri(''); ?>