<?php

	if(!is_user_logged_in()) :

		wp_redirect(site_url('/wp-admin'));
	endif;

	 get_header(); 
	 $form = get_field('form');


 ?>


 	<!-- Add Post -->

<div class="add_post">	
	<div class="container">
		<h3><?php the_title(); ?></h3>
		
		<hr class="line" align="left">
	
		<form 
			id="post_form"
			action="add_post.php" 
			method="post"
			enctype="multipart/form-data"
			class="" >

			<div class="form-group">
				<label for="title"><?php $form['title']; ?></label>

				<input 
					name="title" 
					type="text" 
					class="form-control" >

			</div>





			<div class="form-group">
				<label for="category_id">Category 
					<small><i class="text-muted"> ( Buy, Sell, Rent )</i></small>
				</label>
					
				<select 
				 	value="<?php $form['category']; ?>"
					name="category_id" 
					class="form-control">

					<option value="<?php $form['category']; ?>">	</option>	
				
				</select>
			</div>






			<div class="form-group">
					<label for="city_id">City
						<small><i class="text-muted">  ( Select from covered cities )</i></small>
					</label>

					<select 
						name="city_id" 
						class="form-control">

							<option value=""></option>
		
					</select>
		
			</div>






			<div class="form-group">
				<label for="address">Address</label>

				<input 
					name="address" 
					type="text" 
					placeholder="House/Building no. | Street name" 
					class="form-control" >

			</div>






			<div class="form-group">
				<label for="description">Description
					<small><i class="text-muted">  ( Add as much details as possible )</i></small>
				</label>

				<textarea 
					name="description" 
					class="form-control" >
				
				</textarea>
			</div>

			<div class="form-group">
				<label for="cost">Cost</label>

				<input 
					name="cost" 
					type="text" 
					class="form-control" >

			</div>






			<div class="form-group">
				<label for="contact">Contact details</label>

				<input 
					name="contact" 
					type="text" 
					placeholder="Email or mobile number" 
					class="form-control" >

			</div>






			<div class="form-group">
				<label for="image">Images</label>

				<input 
					name="image" 
					type="file" 
					class="form-control" >
			</div>




		

			<div class="form-group ">
				<button 
					name="post" 
					type="submit"
					class="btn btn-light" >
							 
					Post
				</button>
			</div>

		</form>

	</div>

</div>







<?php get_footer(); ?>

