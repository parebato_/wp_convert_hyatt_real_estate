<?php 

/*Template Name: Properties Page*/


get_header();

$banner = get_field('banner');

 ?>



	<section 
		class="prod-results banner prod_banner_results"
		style="background-image: url(<?= $banner['image']['url']; ?>);">

		<div class="banner_caption_container">
			<div class="banner_caption">

					<h1><?= $banner['heading']; ?></h1>
				
						<p>
							<?= $banner['caption']; ?> 
						</p>
			</div>
		</div>
	</section>
 



	<section class="property-list">
		<div class="content-wrapper">

			<div class="row justify-content-start">
				<?php 
					$property_list = new WP_Query(array(
						'post_type' => 'listings',
						'posts_per_page' => -1,

					));

					while ($property_list->have_posts()) : $property_list->the_post();
						$list = get_field('single_properties');

				?>


				<div class="col-lg-4 col-md-6">
					<div class="property-intro">

						<a href="<?php the_permalink(); ?>">

							<img class="bg" src="<?php the_post_thumbnail_url('property_thumbnail');?>">
								
							<div class="caption-container a-position">
								<div class="caption">
									<h2><?php the_title(); ?></h2>
										<span class="head"><?= $list['city']; ?></span>
								</div>
							</div>

							<div class="status">
							<?php 

								if($list['status'] == "Sold") : ?>									
								<p>
									<i class="red far fa-circle"></i> 
									<strong><?php echo $list['status']; ?></strong>
								</p>
									<?php endif;

								if($list['status'] == "For Bidding") : ?>
								<p>
									<i class="orange far fa-circle"></i> 
									<strong><?php echo $list['status']; ?></strong>
								</p>
									<?php endif; 

								if($list['status'] == "Available" || $list['status'] == "") : ?>										
								<p>
									<i class="green far fa-circle"></i> 
									<strong><?php echo $list['status']; ?></strong>
								</p>
									<?php 

							endif; ?>
							 
							</div>


							<a href="<?php the_permalink(); ?>" class="view a-position">
								<span class="hidden">View</span>
								<img class="arrow" src="<?= get_theme_file_uri('/img/properties/drop-arrow2.png'); ?>">
							</a>

						</a>

					</div>
				</div>
					
			<?php

	endwhile;

	wp_reset_query();

	 ?>

		</div>
		
		</div>
	</section>


	<section class="services-cx charity banner charity-banner">
		<div class="caption-container">
			<div class="caption">
				<h2>
					What’s more, a percentage of the <span class="banner">1% fee goes</span> straight to supporting local charities. Our chosen charities help homeless people, including struggling young people, and this is at the heart of what Hyatt Homes is trying to do.
				</h2>


				<p>
					If you already have a charity that’s close to your heart, let us know, <br>
					and we’ll make sure that they receive the percentage instead.
				</p>
			</div>
		</div>
	</section>



<?php get_footer(); ?>

