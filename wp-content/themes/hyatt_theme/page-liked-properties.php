<?php  

get_header();

/* Template Name: Liked Properties */

?>





<section class="saved_properties">
	
	<div class="container">

		<h2>Liked Properties</h2>

		<div class="liked_properties">

			<div class="row">

				<div class="col-md-4">

					<div class="block">
						<a class="d-flex" href="">

							<img src="<?php echo get_theme_file_uri( 'img/properties/floorplan.png'); ?>">

							<div class="details">
								<h4 class="title my-1"><?php the_title(); ?></h4>
								<p class="mb-2"> Status: <span class="green">Available</span> </p>					
							</div>
						</a>					
					</div>

				</div>
		
			</div>						
		</div>
	</div>
</section>




<?php get_footer(); ?> 
