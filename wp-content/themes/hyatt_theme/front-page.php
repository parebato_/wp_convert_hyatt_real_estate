	<?php 
	get_header(); 

	$banner = get_field('banner');
	$profit = get_field('profit');
	$expertise = get_field('expertise');
	$git = get_field('git');

	?>



   
	<!--End of Main Nav -->
	<div class="home">
		<section 
				class="banner home_banner" 
				style="background-image: url(<?= $banner['image']['url']; ?>);">

			<div>
				<img 
						class="polygon1 a-position" 
						src="<?= get_theme_file_uri('/img/home/polygon1.png') ?>" >
			</div>


			<div class="banner_caption_container">
				<div class="banner_caption mx-3">
					<span class="">AFFORDABLE, ETHICAL, ENABLING</span>

					<h1><?= $banner['heading']; ?></h1>
					<p><?= $banner['caption']; ?></p>	
					
				</div>
			</div>

			<div class="scroll-container">
				<div class="scroll a-position">
					<img src="<?= get_theme_file_uri('/img/services/arrow0.png'); ?>" >
				</div>
			</div>
		</section>
		<!-- *********END OF BANNER ********* -->



		<section class="home_profit">
			<div class="h-arrow1 a-position">
				<img class="img-fluid arrow" src="<?= get_theme_file_uri('/img/home/angle_up.png'); ?>" >
			</div>
			<div class="h-arrow2 a-position">
				<img class="img-fluid arrow" src="<?= get_theme_file_uri('/img/home/angle_up01.png'); ?>" >
			</div>

			<div class="caption-container">
				<div class="caption a-position mx-3">
					<span class="head">SO HOW DOES IT WORK?</span>
					<h2>
						<?= $profit['caption_one']; ?><span class="dot"></span><br>		
					</h2>
					<div class="one-percent">
						<img class="img-fluid" src="<?= get_theme_file_uri('/img/home/one_percent.png'); ?>" >
					</div>
					<p><?= $profit['caption_two']; ?></p>
				</div>
			</div>

		</section>




		<section class="profit_light">
			<div class="caption-container">
				<div class="row mx-3">
					<div class="col-md-6 row-content my-4">
						<img class="hand_icon" src="<?= get_theme_file_uri('/img/home/hand_icon01.png'); ?>" >
						<div class="caption">
							<h2>THERE ARE NO HIDDEN COSTS</h2>
							<p><?= $profit['left']; ?></p>
						</div>
					</div>

					<div class="col-md-6 row-content my-4">
						<img class="hand_icon" src="<?= get_theme_file_uri('/img/home/hand_icon01.png'); ?>" >
						<div class="caption">
							<h2>WE'RE HERE TO HELP</h2>
							<p><?= $profit['right']; ?></p>
						</div>
					</div>
				</div>
			</div>
		</section>




		<section class="hyat_work">

			<div class="caption-container">

				<div class="caption">
					<span class="head">WHY HYATT HOMES?</span>
					<h2><?= $expertise['heading']; ?></h2>
				</div>


				<div class="row">
					<div class="col-lg-4 col-md-6 col-sm-12 row-content">
						<img src="<?= get_theme_file_uri('/img/home/house_vector01.png'); ?>" >
						<div class="caption">

							<h3><?= $expertise['expertise_one']['title']; ?></h3>
							<p><?= $expertise['expertise_one']['content']; ?></p>
							
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 row-content in-between">
						<img src="<?= get_theme_file_uri('/img/home/house_vector02.png'); ?>" >
						<div class="caption">

							<h3><?= $expertise['expertise_two']['title']; ?></h3>
							<p><?= $expertise['expertise_two']['content']; ?></p>
						</div>
					</div>

					<div class="col-lg-4 col-md-6 col-sm-12 row-content">
						<img src="<?= get_theme_file_uri('/img/home/house_vector01.png'); ?>" >
						<div class="caption">

							<h3><?= $expertise['expertise_three']['title']; ?></h3>
							<p><?= $expertise['expertise_three']['content']; ?></p>

						</div>
					</div>
				</div>
			</div>

		</section>
		<!-- *********END OF HYAT_WORK ********* -->

		<hr class="line">

		<div class="aboutUs text-center">
			<a class="head" href="<?= site_url('/about'); ?>">LEARN MORE ABOUT US</a>
		</div>


		<section class="home_search">
			<div class="container">

				<div class="form-arrows">
					<img class="arrow arrow1 a-position" src="<?= get_theme_file_uri('/img/home/form_arrow1.png') ?>" >
					<img class="arrow arrow2 a-position" src="<?= get_theme_file_uri('/img/home/form_arrow2.png') ?>" >
					<img class="arrow arrow3 a-position" src="<?= get_theme_file_uri('/img/home/form_arrow3.png') ?>" >
					<img class="arrow arrow4 a-position" src="<?= get_theme_file_uri('/img/home/form_arrow4.png') ?>" >
					<img class="house_vector a-position" src="<?= get_theme_file_uri('/img/home/home_search_vec.png') ?>" >
				</div>

				<div class="row">
					<div class="col-lg-5 col-md-12">
						<div class="caption">
							<h1>Find your home here</h1>
							<p>
								There are several schemes available to help you buy an affordable home.
								<br>
								Enter your details here to find out if you're eligible.</p>
						</div>
					</div>

					<!-- Form -->

					<div class="col-lg-7 col-md-12 form">
						<form class="">
							<div class="form-row text-center justify-content-between">
								<div class="col-sm-5 input-box">
									<div class="custom-select">
										<select>
											<option value="all" disabled selected>Available Boroughs</option>
											<option value="1">One</option>
											<option value="2">Two</option>
										</select>
									</div>
								</div>
								<div class="col-sm-5 input-box">
									<div class="custom-select">
										<select class="custom-box">
											<option value="all" disabled selected>Your price range</option>
											<option value="1">£0.00</option>
											<option value="2">£0.00</option>
										</select>
									</div>
								</div>
								<div class="col-sm-1 col-xs-12">
									<button type="submit" class="btn btn-info"><i class="fas fa-angle-right"></i></button>
								</div>
							</div> 
						</form>
						<div class="caption">
							<h3>You will need to meet certain criteria to qualify for support.
								<br>
								<a href="#"> Find out if you qualify</a>
							</h3>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- *********END OF HOME_SEARCH ********* -->





<!-- 
		<section class="home_slider">
			<div class="slider-container">
				<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
					<div class="row">
						<div class="col-12">
							<ol class="carousel-indicators mx-4">
								<li data-target="#carouselIndicators" data-slide-to="0"></li>
								<li data-target="#carouselIndicators" data-slide-to="1"></li>
								<li data-target="#carouselIndicators" data-slide-to="2" class="active"></li>
							</ol>
						</div>
					</div>

					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="row justify-content-between mx-3">
								<div class="col-md-6">
									<div class="slider-img">
										<img src="<?= get_theme_file_uri('/img/home/slider1.png'); ?>" alt="First slide">
									</div>
									<div class="caption">
										<h3>Grand opening of Kensington shared <br>ownership 3 bedroom flat</h3>
										<p>The government's announced another £3m for 14 new garden <br>villages across the country, including one in Surrey.</p>
									</div>
								</div>
								<div class="col-md-6 d-sm-none d-md-block d-none">
									<div class="slider-img">
										<img src="<?= get_theme_file_uri('/img/home/slider2.png'); ?>" alt="First slide">
									</div>
									<div class="caption">
										<h3>Hyatt Home's pocket-size solution to <br> help solve housing crisis</h3>
										<p>The government's announced another £3m for 14 new garden <br>villages across the country, including one in Surrey.</p>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section> -->

		<!-- *********END OF HOME_SLIDER ********* -->





	<!-- 	<section class="home_news">
			<hr class="line">
			<div class="container">
				<div class="row justify-content-between mx-1">
					<div class="col-md-6">
						<span class="weeks caption">
							3 WEEKS AGO
						</span>
						<span class="news caption">
							<img class="news-icon" src="<?= get_theme_file_uri('/img/home/news_icon.png'); ?>" >
							<a href="#"> NEW LAUNCHES / NEWS</a>
							<a href="#"><img class="arrow" src="<?= get_theme_file_uri('/img/home/news_arrow.png'); ?>" ></a>
						</span>
					</div>
					<div class="col-md-6">
						<span class="weeks caption">
							3 WEEKS AGO
						</span>
						<span class="news caption">
							<img class="news-icon" src="<?= get_theme_file_uri('/img/home/news_icon.png'); ?>" >
							<a href="#"> NEW LAUNCHES / NEWS</a>
							<a href="#"><img class="arrow" src="<?= get_theme_file_uri('/img/home/news_arrow.png'); ?>" ></a>
						</span>
					</div>
				</div>
			</div>
			<hr class="line">
		</section>
 -->




		<section class="banner git">
			<div class="bg text-center">
				<div class="col-md-12 col-lg-12 col-xl-10 ">
					<div class="caption a-position">
						<h2><?= $git['title']; ?></h2>
						<p><?= $git['caption']; ?></p>
						<div class="button">
							<a class="btn btn-light" href="<?= site_url('/contact') ?>">GET IN TOUCH</a>
						</div>
					</div>
				</div>
			</div>
		</section>


		<div class="tpo">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-5 tpo text-center caption">
						<h2 class="">Working in Partnership with</h2>
						<img src="<?= get_theme_file_uri(''); ?>/img/home/tpo.png" class="tpo_logo" >
					</div>
					<div class="col-md-7 caption">
						<p class="">Like you, we want people to find the right homes.Let's work together to achieve this. Like you, we want people to find the right homes. <a href="">Learn more about our partnership</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
