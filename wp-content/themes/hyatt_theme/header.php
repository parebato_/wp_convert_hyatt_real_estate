
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="<?php bloginfo( 'description' ); ?>">
	<meta name="author" content="">

	<title><?php bloginfo('name'); ?> <?php wp_title(' | '); ?> </title>
	<?php wp_head(); ?>
	<link rel="icon" type="image/png" href="<?= get_theme_file_uri('/img/favicon01.png') ?>">

</head>
 

<body <?php body_class(); ?>>


	<nav class="navbar main-nav fixed-top navbar-expand-lg">
		<a class="navbar-brand" href="<?= site_url(''); ?>">
			<img src="<?= get_theme_file_uri('/img/home/logo.png'); ?>" alt="">

		
		</a>
			<div class="collapse navbar-collapse" id="navCollapse">

				<ul class="navbar-nav mx-auto">

					<li class="overlay-item nav-item active">
						<a class="nav-link" href="<?= site_url('/about'); ?>">About</a>
					</li>


					<li class="overlay-item nav-item ">
						<a class="nav-link" href="<?= site_url('/properties'); ?>">Properties</a>
					</li>


					<li class="overlay-item nav-item ">
						<a class="nav-link" href="<?= site_url('/products'); ?>">Products</a>
					</li>
			
				</ul>


				<div class="overlay-item">

		<?php if(is_user_logged_in()) : ?>

					<!-- Display this section if user is logged in -->
		
					<div class="my-account">

						<p>My Account</p>

						<div class="links">

							<a href="<?= site_url('/liked-properties'); ?>">Liked Properties</a>

							<a href="<?= site_url('/add-post'); ?>">Add Property</a>
							
							<a	
								type="button" 
								href="<?= wp_logout_url(home_url()); ?>">
								LOGOUT	
							</a>

						</div>
						
					</div>
 	
		<?php else : ?>


					<!-- Display Login btn if user is not logged in -->


					<a type="button" 
							class="btn btn-light" 	
							href="<?= wp_login_url(); ?>">
							LOGIN / SIGN UP	
					</a>



																	
		<?php endif; ?>
					
				</div>

			</div>


			<div class="toggle menu" data-toggle="collapse" data-target="#navCollapse">
				<div class="span" id="line01"></div>
				<div class="span" id="line02"></div>
				<div class="span" id="line03"></div>
			</div>

		<hr class="line a-position">
	</nav>

