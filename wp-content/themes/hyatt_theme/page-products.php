
<?php 

get_header();

	$banner = get_field('banner');
	$git = get_field('git');


 ?>



	<div class="products">

		<section class="banner products_banner"
			style="background-image: url(<?= $banner['image']['url']; ?>);">
			<div class="banner_caption_container">

				<div class="banner_caption">
				
						<h1>
							<?= $banner['heading']; ?>
						</h1>
				
						<p>
							<?= $banner['caption']; ?>
						</p>
												
				</div>


			</div>

			<div class="bottom-section">

				<div class="row">
					<div class="col-lg-9 col-md-8 caption">
						<h2>
							Enter your details to see your eligible options.
						</h2>
						<p>
							We’ll discuss the different approaches with you; however to get you started,
							<br>here’s an introduction to affordable housing schemes.
						</p>
					</div>

					<div class="col-lg-3 col-md-4 reg-btn">
						<a class="btn btn-light" href="signUp.php">REGISTER RIGHT NOW</a>
					</div>
				</div>
			</div>


		</section>


		<!-- ********* START | CIRCLES ********* -->
		<section class="circles">

			<div class="arrows">

				<img class="arrow1 a-position" src="<?=get_theme_file_uri('img/products/white_arrow.png'); ?>" alt="">
				<img class="arrow2 a-position" src="<?=get_theme_file_uri('img/products/angle01.png'); ?>" alt="">
				<img class="arrow3 a-position" src="<?=get_theme_file_uri('img/products/angle_transparent.png'); ?>" alt="">

			</div>

			<div class="circle-container">

				<div class="caption">
					<h2>The government's announced another £3M for 14 new garden villages <br> across the country, including one in Surrey.
					</h2>
				</div>

				<div class="row circles-content">

					<div class="col-md-4 col-sm-12 c c-1">
						<a href="">
							<img src="<?=get_theme_file_uri('img/products/shared.png'); ?>" alt="">
							<span>
								Shared<br>ownership
							</span>
						</a>
					</div>

					<div class="col-md-4 col-sm-12 c c-2">
						<a href="">
							<img src="<?=get_theme_file_uri('img/products/help.png'); ?>" alt="">
							<span>
								Help<br>to Buy
							</span>
						</a>
					</div>

					<div class="col-md-3 col-sm-12 c c-3">
						<a href="">
							<img src="<?=get_theme_file_uri('img/products/rentNow.png'); ?>" alt="">
							<span>
								Rent Now,<br>Buy Later
							</span>
						</a>
					</div>

					<div class="col-md-2 col-sm-12 c c-4">
						<a href="">
							<img src="<?=get_theme_file_uri('img/products/equity.png'); ?>" alt="">
							<span>
								Shared<br>Equity
							</span>
						</a>
					</div>

					<div class="col-md-4 col-sm-12 c c-5">
						<a href="">
							<img src="<?=get_theme_file_uri('img/products/rentNowbig.png'); ?>" alt="">
							<span>
								Rent Now,<br>Buy Later
							</span>
						</a>
					</div>


					<div class="col-md-3 col-sm-12 c c-6">
						<a href="">
							<img src="<?=get_theme_file_uri('img/products/discount.png'); ?>" alt="">
							<span>
								Discount<br>full ownership
							</span>
						</a>
					</div>

					<div class="col-md-3 col-sm-12 c c-7">
						<a href="">
							<img src="	<?=get_theme_file_uri('img/products/newham.png'); ?>" alt="">
							<span>
								NewShare<br>(in Newham only)
							</span>
						</a>
					</div>

					<div class="col-md-4 col-sm-12 c c-8">
						<a href="">
							<img src="<?=get_theme_file_uri('img/products/intermidiate.png'); ?>" alt="">
							<span>
								Intermediate rent
							</span>
						</a>
					</div>

					<div class="dots">
						<span class="dot1 a-position donut"> </span>
						<span class="dot2 a-position donut"> </span>
						<span class="dot3 a-position dot"> </span>
						<span class="dot4 a-position dot"> </span>
						<span class="dot5 a-position dot"> </span>
						<span class="dot6 a-position dot"> </span>
					</div>

				</div>

			</div>

			<div class="arrows">
				<img class="arrow4 a-position" src="<?=get_theme_file_uri('img/products/white_arrow01.png'); ?>" alt="">
				<img class="arrow5 a-position" src="<?=get_theme_file_uri('img/products/angle02.png'); ?>" alt="">
			</div>

		</section>


		<!-- ********* END | CIRCLES ********* -->

		<section class="banner git">
			<div class="bg text-center">
				<div class="col-md-12 col-lg-12 col-xl-10 ">
					<div class="caption">
						<h2>
								<?= $git['title']; ?>
						</h2>
						<p>
							<?= $git['caption']; ?>
						</p>
						<div class="button">
							<a class="btn btn-light" href="#">GET IN TOUCH</a>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>



	<?php get_footer(); ?>