

jQuery(document).ready(function($){


	$(".liker").on('click',function(e){
		e.preventDefault();

		var like = $(".liker");
		var like_count = $(".like-count");
		
		

		 if(like.attr('data-exists') == 'yes'){	//DELETE LIKE

		 
		 
		 		$.ajax({
		 			beforeSend: (xhr) => {
		 				xhr.setRequestHeader('X-WP-Nonce', hyatt_data.nonce)
		 			},
		 			url: hyatt_data.root_url + '/wp-json/hyatt/v1/likes',
		 			data: {'like': like.attr('data-like')}, 		
		 			type: 'DELETE',
		 			success: (res) => {

		 				like.attr('data-exists', 'no');
		 				like.attr('data-like', '');

		 				var thumbsUp = like.find('i');
		 				thumbsUp.removeClass('liked');

		 				var count = parseInt(like_count.html(), 10);
		 				count--;
		 				like_count.html(count);

		 				console.log(res);
		 			},
		 			error: (res) => {
		 				console.log(res);
		 			}

		 	});





		 	
		 } else {	//CREATE LIKE
	 
	 		

		 	$.ajax({
		 			beforeSend: (xhr) => {
		 				xhr.setRequestHeader('X-WP-Nonce', hyatt_data.nonce)
		 			},
		 			url: hyatt_data.root_url + '/wp-json/hyatt/v1/likes',
		 			data: {
		 				'property_id' : like.data('property')
		 			},		 			
		 			type: 'POST',
		 			success: (res) => {
		 				//if successful , increase like count, and add css to icon dynamically

						like.attr('data-like', 'res');
		 				like.attr('data-exists', 'yes');

		 				var thumbsUp = like.find('i');
		 				thumbsUp.addClass('liked');

		 				var count = parseInt(like_count.html(), 10);
		 				count++;
		 				like_count.html(count);		

		 				console.log(res); 				

		 			},
		 			error: (res) => {
		 				console.log(res);
		 			}

		 	});






		 }//end bracket for else statement

	});
	

});



/*
Note: 

 1. ajax data handled by inc/likes-rote.php
 2. create a script to get the dynamic user/page id to be used in the likes-route.php syntax



Delete like
	1. Single Property Page - create indication that page is currently liked. Echo out created $exist_query from likes-route.php in html.
*/