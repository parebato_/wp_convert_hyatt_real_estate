


 jQuery(document).ready(function($){


  	// MAIN-NAVBAR COLOR TRANSITION //

  	$(window).scroll(function () {
  		if ($(document).scrollTop() > 25) {
  			$(".main-nav").css("background-color", "#115a71", );
  		} else {
  			$(".main-nav").css("background-color", "transparent");
  		}
  	});





  	// STICKY-NAV ON PROPERTIES

  	$(window).scroll(function () {
  		var sticky = $(".sticky"),
  			scroll = $(window).scrollTop();

  		if (scroll >= 750) sticky.addClass("fixed");
  		else sticky.removeClass("fixed");
  	});






  	// Main Nav Overlay toggle btn

  	
  	$(".menu").click(function () {
  		$(this).toggleClass("close");
  	});




  
 });

