


//Check if password match in register confirm password

	function pwdMatch(){

		var password = $("#password").val();
		var password_confirmation = $("#password_confirmation").val();

		if(password != password_confirmation)
			$("#check_password").html("( Passwords do not match )").css("color", "red");


		else
			$("#check_password").html("( Passwords match )").css("color", "green");
	}

	$(document).ready(function(){
		$("#password_confirmation").keyup(pwdMatch);
	});






//Check if username is already taken

	function checkUsernameAvailability(){

		$('#loaderIcon').show();

				$.ajax({
						url:'page-register.php',
						data:'username='+ $("#username").val(),
						type: 'POST',
						success: function(data){


							$("#username_message").html(data);
							$("#loaderIcon").hide();
						},
						error:function (){
					}
				});

			}


//Check if email already exists

	function checkEmailAvailability(){
		// $('#loaderIcon').show();

		$.ajax({
				url:'page-register.php',
				data:'email='+ $("#email").val(),
				type: 'POST',
				success: function(data){
					$("#email_message").html(data);
					$("#loaderIcon").hide();
				},
				error:function (){
					// event.preventDefault();
			}
		});
	}








