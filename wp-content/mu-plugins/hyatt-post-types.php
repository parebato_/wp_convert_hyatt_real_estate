<?php

function hyatt_post_types(){

	//List Post type

	register_post_type('listings', array(
		'show_in_rest' 			=> true,
		'has_archive' 			=> true,
		'public' 						=> true, 
		'menu_icon' 				=> 'dashicons-admin-multisite',
		'labels' 						=> array(
			'name' 						=> 'Listings',
			'add_new_item' 		=> 'Add New List',
			'edit_item' 			=> 'Edit List',
			'all_items' 			=> 'All Listings', 
			'singular_name' 	=> 'List'
		),
		'supports' 					=> array(
			'title',
			'thumbnail',
			'author'
		)
	));




	//Like Post type
	
	register_post_type('likes', array(
		'public' 					=> true, 
		'show_ui' 				=> true,
		'publicly_queryable' => true,
		'menu_icon' 			=> 'dashicons-thumbs-up',
		'labels' 					=> array(
			'name' 					=> 'Likes',
			'add_new_item'	=> 'Add New Like', 
			'edit_item' 		=> 'Edit Like',
			'all_items' 		=> 'All Likes', 
			'singular_name' => 'Like'
		),
		'supports' 				=> array(
			'title',
			'author',
			'thumbnail'
		)
	));

}


add_action('init', 'hyatt_post_types');

